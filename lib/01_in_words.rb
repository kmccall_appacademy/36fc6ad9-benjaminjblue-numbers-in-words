class Fixnum

  def in_words
    int = self
    if int == 0
      return "zero"
    end
    ones_and_tens = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]
    twenty_until_100 = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"].map do |tens_place|
      (0..9).map {|ones| tens_place + " " + (ones == 0 ? "" : ones_and_tens[ones - 1])}
    end
    below_100 = ones_and_tens + twenty_until_100.flatten
   thousands = ["trillion", "billion", "million", "thousand", ""]
   words = ""
    while int / 1000.0 > 0
      num = int % 1000
      word = ""
      hundred = (num / 100).floor
      if hundred > 0
        word += below_100[hundred - 1] + " hundred "
      end
      if num > 0 && num % 100 != 0
        word += below_100[num % 100 - 1]
      end
      thousand = thousands.pop
      words = word + " " + (word.length > 0 ? thousand : "") + " " + words
      int = (int / 1000).floor
    end
    words.split.join(' ')
  end

end
puts 100.in_words
